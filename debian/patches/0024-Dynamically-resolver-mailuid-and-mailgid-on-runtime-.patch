From: =?utf-8?q?Ond=C5=99ej_Sur=C3=BD?= <ondrej@sury.org>
Date: Thu, 14 May 2020 02:32:42 +0200
Subject: Dynamically resolve mailuid and mailgid on runtime

Dynamically resolve the daemon uid and gid from their names
when configured as zero.

MAILUID / MAILGID are used by code in the following files:
* courier/module.dsn/dsn.c
* courier/courier-config.c
* courier/module.fax/courierfax.c (once per fax message)
* courier/module.local/localmail.c (patched to calculate on init)
* courier/module.local/local.c (one-time initialization for authinfo uid field)
* courier/sendmail.c (MAILGID only)
* courier/module.uucp/uucp.c
* courier/courier.h.in
* courier/module.esmtp/courieresmtp.c
* courier/ldapaliasd.c
* courier/testmxlookup.c
* courier/courierd.C courierbmain
* courier/courier.c
* courier/filters/courierfilter.c
* courier/filters/verifyfilter.c
* courier/submit.C
* courier/courierdb.C (chown of triggername fifo)


---
 courier/Makefile.am          |  7 ++--
 courier/configure.ac         |  8 -----
 courier/courier-config.c     |  1 +
 courier/courier.h.in         |  6 ++--
 courier/courierdb.C          |  1 +
 courier/module.local/local.c |  3 +-
 libs/numlib/changeuidgid.c   | 84 ++++++++++++++++++++++++++++++++++++++++++++
 libs/numlib/numlib.h         |  3 ++
 8 files changed, 98 insertions(+), 15 deletions(-)

--- a/courier/Makefile.am
+++ b/courier/Makefile.am
@@ -341,7 +341,7 @@
 	courierdb.C courierd.C courierd.h mydirent.h
 
 courierd_DEPENDENCIES=$(COURIERDEPENDENCIES) ../afx/libafx.a ../libs/liblock/liblock.la ../libs/tcpd/libspipe.la ../libs/soxwrap/libsoxwrap.a ../libs/soxwrap/soxlibs.dep
-courierd_LDADD=$(COURIERLDADD) ../afx/libafx.a ../libs/liblock/liblock.la ../libs/tcpd/libspipe.la ../libs/soxwrap/libsoxwrap.a `cat ../libs/soxwrap/soxlibs.dep` $(DEBUGLIB) -lcourierauth
+courierd_LDADD=$(COURIERLDADD) ../afx/libafx.a ../libs/liblock/liblock.la ../libs/tcpd/libspipe.la ../libs/numlib/libnumlib.la ../libs/soxwrap/libsoxwrap.a `cat ../libs/soxwrap/soxlibs.dep` $(DEBUGLIB) -lcourierauth
 courierd_LDFLAGS=`@COURIERAUTHCONFIG@ --ldflags`
 
 # tcpd
@@ -433,6 +433,7 @@
 # courier-config
 
 courier_config_SOURCES=courier-config.c
+courier_config_LDADD=../libs/numlib/libnumlib.la
 
 EXTRA_DIST= testsuite.alias testsuite.alias1.in testsuite.alias2.in \
 		aliases.config smtpaccess.config \
--- a/courier/courier-config.c
+++ b/courier/courier-config.c
@@ -16,6 +16,7 @@
 #include	"configargs.h"
 #include	<string.h>
 #include	<stdlib.h>
+#include	"numlib/numlib.h"
 #if	HAVE_UNISTD_H
 #include	<unistd.h>
 #endif
--- a/courier/courier.h.in
+++ b/courier/courier.h.in
@@ -16,11 +16,12 @@
 extern "C" {
 #endif
 
-#define	MAILUID		@mailuid@
-#define	MAILGID		@mailgid@
 #define	MAILUSER	"@mailuser@"
 #define	MAILGROUP	"@mailgroup@"
 
+#define	MAILUID		( (@mailuid@) ? (@mailuid@) : libmail_getuid(MAILUSER, 0) )
+#define	MAILGID		( (@mailgid@) ? (@mailgid@) : libmail_getgid(MAILGROUP) )
+
 #define	COURIER_HOME	"@prefix@"
 
 #define	COURIER_COPYRIGHT	"@COPYRIGHT@"
--- a/courier/courierdb.C
+++ b/courier/courierdb.C
@@ -25,6 +25,7 @@
 #include	"waitlib/waitlib.h"
 #include	"liblock/config.h"
 #include	"liblock/liblock.h"
+#include	"numlib/numlib.h"
 
 pid_t	couriera, courierb;
 time_t	courierastart, courierbstart;
--- a/courier/module.local/local.c
+++ b/courier/module.local/local.c
@@ -27,6 +27,7 @@
 #include	"sysconfdir.h"
 #include	<courierauth.h>
 #include	<idna.h>
+#include	"numlib/numlib.h"
 
 #if	HAVE_SYSLOG_H
 #include	<syslog.h>
@@ -365,7 +366,8 @@
 
 	{
 	struct	authinfo aa;
-	static const uid_t	mailuid=MAILUID;
+	static /*const*/ uid_t	mailuid;
+	if ( !mailuid ) mailuid=MAILUID;
 
 		memset(&aa, 0, sizeof(aa));
 		aa.homedir= ALIASDIR;
--- a/libs/numlib/changeuidgid.c
+++ b/libs/numlib/changeuidgid.c
@@ -47,12 +47,12 @@
 	}
 }
 
-void libmail_changeusername(const char *uname, const gid_t *forcegrp)
+/**
+ * Obtain the uid associated to uname and, optionally, the user primary gid
+ */
+uid_t libmail_getuid(const char *uname, gid_t *pw_gid)
 {
 struct passwd *pw;
-uid_t	changeuid;
-gid_t	changegid;
-
 /* uname might be a pointer returned from a previous called to getpw(),
 ** and libc has a problem getting it back.
 */
@@ -74,11 +74,19 @@
 	}
 	free(p);
 
-	changeuid=pw->pw_uid;
+	if ( pw_gid ) *pw_gid = pw->pw_gid;
+
+	return pw->pw_uid;
+}
+
+void libmail_changeusername(const char *uname, const gid_t *forcegrp)
+{
+uid_t	changeuid;
+gid_t	changegid;
 
-	if ( !forcegrp )	forcegrp= &pw->pw_gid;
+	changeuid=libmail_getuid(uname, &changegid);
 
-	changegid= *forcegrp;
+	if ( forcegrp )	changegid= *forcegrp;
 
 	if ( setgid( changegid ))
 	{
@@ -87,7 +95,7 @@
 	}
 
 #if HAVE_INITGROUPS
-	if ( getuid() == 0 && initgroups(pw->pw_name, changegid) )
+	if ( getuid() == 0 && initgroups(uname, changegid) )
 	{
 		perror("initgroups");
 		exit(1);
@@ -108,3 +116,45 @@
 		exit(1);
 	}
 }
+
+gid_t libmail_getgid(const char *gname) {
+	gid_t g;
+	struct group grp;
+	struct group *result;
+	char *buf;
+	size_t bufsize;
+	int s;
+
+	bufsize = sysconf(_SC_GETGR_R_SIZE_MAX);
+	if (bufsize == -1)          /* Value was indeterminate */
+	{
+		bufsize = 16384;        /* Should be more than enough */
+	}
+
+	buf = malloc(bufsize);
+	if (buf == NULL)
+	{
+		perror("malloc");
+		exit(1);
+	}
+
+	s = getgrnam_r(gname, &grp, buf, bufsize, &result);
+	if (result == NULL)
+	{
+		if (s == 0)
+		{
+			fprintf(stderr, "CRIT: Group %s not found\n", gname);
+		}
+		else
+		{
+			errno = s;
+			perror("getpwnam_r");
+		}
+		exit(1);
+	}
+
+	g = grp.gr_gid;
+	free(buf);
+
+	return g;
+}
--- a/libs/numlib/numlib.h
+++ b/libs/numlib/numlib.h
@@ -95,6 +95,9 @@
 	** no aux group IDs for the user, any AUX ids are cleared.
 	*/
 
+uid_t libmail_getuid(const char *, gid_t *);
+gid_t libmail_getgid(const char *);
+
 #ifdef	__cplusplus
 }
 #endif
