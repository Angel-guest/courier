#!/bin/sh

set -e

# Source debconf library
. /usr/share/debconf/confmodule

del_override() {
    if dpkg-statoverride --list $4 >/dev/null; then
	dpkg-statoverride --force --remove $4
    fi
}

apache24_uninstall() {
    if [ -d /etc/apache2/conf-available ] && [ -L /etc/apache2/conf-available/sqwebmail.conf ]; then
        # disable configuration on new install with apache2-mainscript-helper
        if [ -z "$2" ]; then
            if [ -e /usr/share/apache2/apache2-maintscript-helper ] ; then
                . /usr/share/apache2/apache2-maintscript-helper
                apache2_invoke disconf sqwebmail.conf
            fi
        fi

        # remove symlink
        rm /etc/apache2/conf-available/sqwebmail.conf
    fi
}

if [ "$1" = "purge" ]; then
    # remove cache with authentification info
    rm -rf /var/cache/sqwebmail

    db_get sqwebmail/install-www || [ $? -eq 10 ]
    if [ "$RET" = "symlink" ]; then
	rm -f /var/www/sqwebmail
    elif [ "$RET" = "copy" ]; then
	rm -rf /var/www/sqwebmail
    fi

    # remove calendar mode configuration file
    rm -f /etc/courier/calendarmode

    # remove ispelldict configuration
    rm -f /etc/courier/ispelldict

    # possibly disable and unlink apache configuration
    apache24_uninstall "$@"

    del_override root    courier 0755 /usr/lib/courier/courier/webmail
    del_override courier courier 2755 /usr/lib/courier/courier/sqwebpasswd
    del_override courier courier 0700 /var/cache/sqwebmail
    del_override courier courier 0750 /var/lib/courier/calendar
fi

#DEBHELPER#
